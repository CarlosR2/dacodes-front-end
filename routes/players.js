var express = require('express');
var router = express.Router();
var axios = require('axios')
const util = require('../utils/utils');
/* GET home page. */
router.get('/', async function(req, res, next) {
  let { data } =await axios.get('  https://venados.dacodes.mx/api/players', { params:{}, headers: { 'Accept': 'application/json' } })
  let team = data.data.team;
  let players_ = team.goalkeepers.concat(team.defenses).concat(team.centers).concat(team.forwards);
  let players= players_.map(function (item) {
    item.birthdayFormat = util.simpleFormatDate(item.birthday);
    return item;
  })
  console.log(players);
  res.render('players', { title: 'Express', team:players });
});


module.exports = router;

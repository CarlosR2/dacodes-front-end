var express = require('express');
var router = express.Router();
var axios = require('axios')
const util = require('../utils/utils')
/* GET home page. */
router.get('/', async function(req, res, next) {
  let { data } =await axios.get('https://venados.dacodes.mx/api/games', { params:{}, headers: { 'Accept': 'application/json' } })
  let games = data.data.games
  let league = games.filter(function (game) {
      return game.league == 'Ascenso MX';
  }).map(function (item) {
        item.fixDate= util.simpleFormatDate(item.datetime);
        return item;
  })
  res.render('matches', { title: 'Express', games:league });
});


module.exports = router;

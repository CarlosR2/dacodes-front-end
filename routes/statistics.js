var express = require('express');
var router = express.Router();
var axios = require('axios')
const util = require('../utils/utils')
/* GET home page. */
router.get('/', async function(req, res, next) {
  let { data } =await axios.get('https://venados.dacodes.mx/api/games', { params:{}, headers: { 'Accept': 'application/json' } })
  let games = data.data.games
  let league = games.filter(function (game) {
      return game.league == 'Ascenso MX';
  });
  let statistic = {JJ:0,JG:0,JE:0,JP:0,GF:0,GC:0,PTS:0}
  console.log(league)
  league.forEach(function (element) {
      statistic.JJ+=1;
      if (element.home_score >element.away_score){
          if(element.local){
              statistic.JG+=1
              statistic.GF+=element.home_score;
              statistic.GC+=element.away_score;
              statistic.PTS+=3;
          }
          else{
              statistic.JP+=1
              statistic.GC+=element.home_score;
              statistic.GF+=element.away_score;
          }
      }
      else if(element.home_score == element.away_score){
            statistic.JE+=1
            statistic.GC+=element.home_score;
            statistic.GF+=element.away_score;
            statistic.PTS+=1;

      }
      else {
          if(element.local){
              statistic.JP+=1
              statistic.GF+=element.home_score;
              statistic.GC+=element.away_score
          }
          else{
              statistic.JG+=1
              statistic.GC+=element.home_score;
              statistic.GF+=element.away_score;
              statistic.PTS+=3;
          }
      }
  });
  statistic.DF = statistic.GF-statistic.GC
  console.log(statistic)
  res.render('statistics', { title: 'Express', statistics:statistic });
})

module.exports = router;


exports.simpleFormatDate = function (_date){
    let date = _date.split('T');
    let dateSplit = date[0].split('-');
    return dateSplit[2]+"-"+dateSplit[1]+"-"+dateSplit[0];
}
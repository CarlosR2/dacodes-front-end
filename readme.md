# Dacodes Front-end

### Requeriments
    Node js 10.16.0 LTS

### Clone project
    git clone https://gitlab.com/CarlosR2/dacodes-front-end.git

### Move into folder
    cd dacodes-front-end
### Install

    npm i

### Run
    npm start

### Url
    http://localhost:3000
    
Nota:  
 La api de estadisticas devolvia un arreglo vacio, por lo cual se calcularon las estadisticas del equipo en base a los resultados de la liga Ascenso MX